#Rules
+ Please use JAVA or Swift
+ You can use any libraries and frameworks
+ You cannot use any plugins

#Evaluation criterion
+ Any tiny difference from the design, even a few pixel difference, may cause a score deduction.
+ The implementation meets the specification or not
+ Readability and maintainability of the source code