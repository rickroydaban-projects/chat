#Rules
+ Please use JAVA or Swift
+ You can use any libraries and frameworks.

#Evaluation criterion
+ Any tiny difference from the design, even a few pixel difference, may cause a score deduction.
+ The Implementation meets the specification or not