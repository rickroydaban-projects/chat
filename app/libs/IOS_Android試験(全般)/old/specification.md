# Overview

This is the simple chat application.
It is like Messanger in facebook and Direct message in twitter.

# sign-up
It has 3 patterns.
1. when users input no values and click the button "Sigun up / Log in", nothing happened.
2. If users have already had own accounts, when they input correct values and click the button "Sigun up / Log in", they will succeed to log in and transfer to chat page.
3. If users don't have own accounts, when they input any values and click the button "Sigun up / Log in", thir account will be made automatically, succeed to log in and transfer to chat page.

# specification-chat
- Users can post a message by inputting text in the bottom input area and clicking the button "send".
- Each post has only 2 data. They are a message and user's name.
- The more a post is newer, it will come to the bottom.
- The more a post is older, it will come to the top.
- User's own post is always located the right side.
- Other member's post is always located the left side.
- When users click the button "Log out", they will log out and transfer to sign-up page.