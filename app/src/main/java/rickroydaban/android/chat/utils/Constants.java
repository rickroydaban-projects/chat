package rickroydaban.android.chat.utils;

/**
 * Created by rickroydaban on 01/08/2018.
 */

public class Constants {
    public static final int USERNAME_LENGTH_MIN = 8;
    public static final int USERNAME_LENGTH_MAX = 16;
    public static final int PASSWORD_LENGTH_MIN = 8;
    public static final int PASSWORD_LENGTH_MAX = 16;

    public static int LOAD_TIMEOUT = 12000;

}
