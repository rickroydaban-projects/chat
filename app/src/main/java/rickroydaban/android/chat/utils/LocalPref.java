package rickroydaban.android.chat.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by rickroydaban on 01/08/2018.
 */

public class LocalPref {
    private static LocalPref instance;

    private final String USER_KEY = "user";
    private final String USER_NAME = "name";


    public static void init(Context context){
        if(instance == null){
            instance = new LocalPref(context);
        }
    }

    public static LocalPref get(){ return instance; }

    private Context context;
    private SharedPreferences pref;

    private LocalPref(Context context){
        this.context = context;
        pref = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void login(String userKey, String accessToken){
        pref.edit().putString(USER_KEY, userKey).putString(USER_NAME, accessToken).commit();
    }

    public void logout(){
        pref.edit().clear().commit();
    }

    public String getKey(){ return pref.getString(USER_KEY, ""); }
    public String getName(){ return pref.getString(USER_NAME, ""); }}
