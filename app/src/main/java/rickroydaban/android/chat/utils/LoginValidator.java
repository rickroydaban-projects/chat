package rickroydaban.android.chat.utils;

/**
 * Created by rickroydaban on 01/08/2018.
 */

public class LoginValidator {

    public LoginValidator(){}

    public boolean isUsernameEmpty(String username){
        if(username.length() == 0) {
            return true;
        }else{
            return false;
        }
    }

    public boolean isPasswordEmpty(String password){
        if(password.length() == 0) {
            return true;
        }else{
            return false;
        }
    }

    public boolean isUsernameCorrectLength(String username){
//        if(username.length() >= Constants.USERNAME_LENGTH_MIN && username.length() <= Constants.USERNAME_LENGTH_MAX){
        if(username.length() >= Constants.USERNAME_LENGTH_MIN){
            return true;
        }else{
            return false;
        }
    }

    public boolean isPasswordCorrectLength(String password){
        if(password.length() >= Constants.PASSWORD_LENGTH_MIN && password.length() <= Constants.PASSWORD_LENGTH_MAX){
            return true;
        }else{
            return false;
        }
    }

}
