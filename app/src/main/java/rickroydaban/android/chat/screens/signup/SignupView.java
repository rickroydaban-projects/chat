package rickroydaban.android.chat.screens.signup;

/**
 * Created by rickroydaban on 01/08/2018.
 */

public interface SignupView {
    void signup();
    void login();
    void onSignupSuccess();
    void onSignupFailed(String message);
    void onUsernameIncorrect(String message);
    void onPasswordIncorrect(String message);
}
