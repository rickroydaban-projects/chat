package rickroydaban.android.chat.screens.signup;

import android.content.Context;

/**
 * Created by rickroydaban on 01/08/2018.
 */

public interface SignupPresenter {
    void init(Context context);
    void signup(String username, String password);
}
