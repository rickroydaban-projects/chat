package rickroydaban.android.chat.screens.chat.models;

public class TheirChatMessage extends ChatMessage{
    private String username;

    public TheirChatMessage(long date, String content, String username) {
        super(date, content);
        this.username = username;
    }

    public String getUsername(){
        return username;
    }
}
