package rickroydaban.android.chat.screens.login.models;

import rickroydaban.android.chat.models.User;

/**
 * Created by rickroydaban on 01/08/2018.
 */

public class AuthUser extends User {

    public AuthUser(String userKey, String username) {
        super(userKey, username);
    }

}
