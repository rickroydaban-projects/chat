package rickroydaban.android.chat.screens.login;

import android.content.Context;

import rickroydaban.android.chat.R;
import rickroydaban.android.chat.callbacks.GenericCallback;
import rickroydaban.android.chat.screens.login.models.AuthUser;
import rickroydaban.android.chat.utils.LocalPref;
import rickroydaban.android.chat.utils.LoginValidator;
import rickroydaban.android.chat.webservice.Webservice;

/**
 * Created by rickroydaban on 01/08/2018.
 */

public class LoginPresenterImpl implements LoginPresenter {
    private LoginView view;
    private Webservice webservice;
    private LoginValidator loginValidator;
    private Context context;

    public LoginPresenterImpl(LoginView loginView, Webservice webservice){
        this.view = loginView;
        this.webservice = webservice;
        loginValidator = new LoginValidator();
    }

    @Override
    public void init(Context context) {
        this.context = context;
    }

    @Override
    public void login(String username, String password) {
        boolean isUsernameOk = false;
        boolean isPasswordOk = false;
        if(!loginValidator.isUsernameEmpty(username) && loginValidator.isUsernameCorrectLength(username)){
            isUsernameOk = true;
        }else{
            view.onUsernameIncorrect(context.getResources().getString(R.string.value_is_incorrect));
        }

        if(!loginValidator.isPasswordEmpty(password) && loginValidator.isPasswordCorrectLength(password)){
            isPasswordOk = true;
        }else{
            view.onPasswordIncorrect(context.getResources().getString(R.string.value_is_incorrect));
        }

        if(isUsernameOk && isPasswordOk){
            webservice.login(username, password, new GenericCallback() {
                @Override
                public <T> void onSuccess(T result) {
                    AuthUser user = (AuthUser) result;
                    LocalPref.get().login(user.getUserKey(), user.getUsername());
                    view.onLoginSuccess();
                }

                @Override
                public void onFailure(String message) {
                    view.onLoginFailed(message);
                }
            });
        }
    }
}
