package rickroydaban.android.chat.screens.chat;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rickroydaban.android.chat.R;
import rickroydaban.android.chat.screens.chat.models.ChatMessage;
import rickroydaban.android.chat.screens.chat.models.MyChatMessage;
import rickroydaban.android.chat.screens.chat.models.TheirChatMessage;

public class ChatAdapter extends RecyclerView.Adapter {
    private final int MESSAGE_MINE = 0;
    private final int MESSAGE_THEIRS = 1;

    private List<ChatMessage> chatMessages;

    public ChatAdapter(List<ChatMessage> chatMessages){
        this.chatMessages = chatMessages;
    }

    @Override
    public int getItemViewType(int position) {
        if(chatMessages.get(position) instanceof MyChatMessage){
            return MESSAGE_MINE;
        }else{
            return MESSAGE_THEIRS;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == MESSAGE_MINE){
            return new HolderMyMessage(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_message_mine, parent, false));
        }else{
            return new HolderTheirMessage(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_message_theirs, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ChatMessage chatMessage = chatMessages.get(position);
        if(chatMessage instanceof MyChatMessage){
            HolderMyMessage holder = (HolderMyMessage)viewHolder;
            MyChatMessage myChatMessage = (MyChatMessage)chatMessage;
            holder.tvContent.setText(myChatMessage.getContent());
        }else{
            HolderTheirMessage holder = (HolderTheirMessage)viewHolder;
            TheirChatMessage theirChatMessage = (TheirChatMessage)chatMessage;
            holder.tvContent.setText(theirChatMessage.getContent());
            String email = theirChatMessage.getUsername();
            holder.tvUsername.setText(email.split("@")[0]);
        }
    }

    @Override
    public int getItemCount() {
        return chatMessages.size();
    }

    public class HolderMyMessage extends RecyclerView.ViewHolder{
        @BindView(R.id.tviews_message_content) TextView tvContent;

        public HolderMyMessage(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class HolderTheirMessage extends RecyclerView.ViewHolder{
        @BindView(R.id.tviews_message_content) TextView tvContent;
        @BindView(R.id.tviews_message_username) TextView tvUsername;

        public HolderTheirMessage(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
