package rickroydaban.android.chat.screens.main;

/**
 * Created by rickroydaban on 01/08/2018.
 */

public interface MainView {
    void navigateToLogin();
    void navigateToSignup();
}
