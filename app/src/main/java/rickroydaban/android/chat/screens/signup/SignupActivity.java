package rickroydaban.android.chat.screens.signup;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rickroydaban.android.chat.DependencyAssembly;
import rickroydaban.android.chat.R;
import rickroydaban.android.chat.screens.login.LoginActivity;
import rickroydaban.android.chat.screens.login.LoginPresenter;
import rickroydaban.android.chat.screens.login.LoginView;
import rickroydaban.android.chat.screens.signup.SignupPresenter;
import rickroydaban.android.chat.screens.signup.SignupView;

/**
 * Created by rickroydaban on 01/08/2018.
 */

public class SignupActivity extends AppCompatActivity implements SignupView {
    @BindView(R.id.etexts_username) EditText etUsername;
    @BindView(R.id.etexts_password) EditText etPassword;
    @BindView(R.id.tviews_username_error) TextView tvUsernameError;
    @BindView(R.id.tviews_password_error) TextView tvPasswordError;

    private SignupPresenter presenter;
    private ProgressDialog pd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        presenter = DependencyAssembly.get().signupPresenter(this);
        presenter.init(this);

        pd = new ProgressDialog(this);
        pd.setMessage(getResources().getString(R.string.loading));
    }

    @OnClick(R.id.buttons_login) public void onLoginButtonClick(){
        login();
    }

    @OnClick(R.id.buttons_signup) public void onSignupButtonClick(){
        signup();
    }

    @Override
    public void signup() {
        pd.show();
        tvUsernameError.setVisibility(View.GONE);
        tvPasswordError.setVisibility(View.GONE);
        presenter.signup(etUsername.getText().toString(), etPassword.getText().toString());
    }

    @Override
    public void login() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onSignupSuccess() {
        pd.dismiss();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onSignupFailed(String message) {
        pd.dismiss();
        new AlertDialog.Builder(this).setTitle("").setMessage(message)
                .setNegativeButton(R.string.dismiss, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();
    }

    @Override
    public void onUsernameIncorrect(String message) {
        pd.dismiss();
        tvUsernameError.setVisibility(View.VISIBLE);
        tvUsernameError.setText(message);
    }

    @Override
    public void onPasswordIncorrect(String message) {
        pd.dismiss();
        tvPasswordError.setVisibility(View.VISIBLE);
        tvPasswordError.setText(message);

    }
}
