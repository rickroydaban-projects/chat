package rickroydaban.android.chat.screens.chat;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rickroydaban.android.chat.DependencyAssembly;
import rickroydaban.android.chat.R;
import rickroydaban.android.chat.screens.chat.ChatPresenter;
import rickroydaban.android.chat.screens.chat.ChatView;
import rickroydaban.android.chat.screens.main.MainActivity;

public class ChatActivity extends AppCompatActivity implements ChatView {
    @BindView(R.id.recyclerview) RecyclerView recyclerView;
    @BindView(R.id.etexts_message) EditText etMessage;
    @BindView(R.id.buttons_send) Button btSend;
    @BindView(R.id.cs_loading) View cLoading;

    private ChatPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        presenter = DependencyAssembly.get().chatPresenter(this);
        presenter.init(this);
    }

    @OnClick(R.id.buttons_send) public void onSendButtonClick(){
        sendMessage();
    }

    @OnClick(R.id.tviews_logout) public void onLogoutClick(){
        new AlertDialog.Builder(this).setTitle("").setMessage(getResources().getString(R.string.logout_confirm))
                .setPositiveButton(R.string.logout, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        logout();
                    }
                }).setNegativeButton(R.string.dismiss, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create().show();
    }

    @Override
    public void onMessageProcessed() {
        etMessage.setText("");
        etMessage.setEnabled(true);
        btSend.setText(getResources().getString(R.string.send));
        btSend.setEnabled(true);
    }

    @Override
    public void updateAdapter(ChatAdapter adapter) {
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void sendMessage() {
        etMessage.setEnabled(false);
        btSend.setText(getResources().getString(R.string.sending));
        btSend.setEnabled(false);

        presenter.sendMessage(etMessage.getText().toString());
    }

    @Override
    public void logout() {
        presenter.logout();
    }

    @Override
    public void onSendMessageSuccess() {
        onMessageProcessed();
        Toast.makeText(this, getResources().getString(R.string.message_sent_successfully), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSendMessageFailure(String message) {
        onMessageProcessed();
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGetConversationSuccess() {
        cLoading.setVisibility(View.GONE);
        recyclerView.scrollToPosition(recyclerView.getAdapter().getItemCount()-1);
    }

    @Override
    public void onGetConversationFailed(String message) {
        cLoading.setVisibility(View.GONE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLogoutSuccess() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
