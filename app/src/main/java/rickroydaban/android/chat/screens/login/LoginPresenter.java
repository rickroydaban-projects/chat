package rickroydaban.android.chat.screens.login;

import android.content.Context;

/**
 * Created by rickroydaban on 01/08/2018.
 */

public interface LoginPresenter {
    void init(Context context);
    void login(String username, String password);
}
