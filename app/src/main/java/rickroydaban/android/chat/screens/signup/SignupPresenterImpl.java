package rickroydaban.android.chat.screens.signup;

import android.content.Context;

import rickroydaban.android.chat.R;
import rickroydaban.android.chat.callbacks.EmptyCallback;
import rickroydaban.android.chat.utils.LoginValidator;
import rickroydaban.android.chat.webservice.Webservice;

/**
 * Created by rickroydaban on 01/08/2018.
 */

public class SignupPresenterImpl implements SignupPresenter {
    private SignupView view;
    private Webservice webservice;
    private LoginValidator loginValidator;
    private Context context;

    public SignupPresenterImpl(SignupView view, Webservice webservice){
        this.view = view;
        this.webservice = webservice;
        loginValidator = new LoginValidator();
    }

    @Override
    public void init(Context context) {
        this.context = context;
    }

    @Override
    public void signup(String username, String password) {
        boolean isUsernameOk = false;
        boolean isPasswordOk = false;
        if(!loginValidator.isUsernameEmpty(username) && loginValidator.isUsernameCorrectLength(username)){
            isUsernameOk = true;
        }else{
            view.onUsernameIncorrect(context.getResources().getString(R.string.value_is_incorrect));
        }

        if(!loginValidator.isPasswordEmpty(password) && loginValidator.isPasswordCorrectLength(password)){
            isPasswordOk = true;
        }else{
            view.onPasswordIncorrect(context.getResources().getString(R.string.value_is_incorrect));
        }

        if(isUsernameOk && isPasswordOk){
            webservice.signup(username, password, new EmptyCallback() {
                @Override
                public void onSuccess() {
                    view.onSignupSuccess();
                }

                @Override
                public void onFailure(String message) {
                    view.onSignupFailed(message);
                }
            });
        }
    }
}
