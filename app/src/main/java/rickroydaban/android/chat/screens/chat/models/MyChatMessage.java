package rickroydaban.android.chat.screens.chat.models;

import rickroydaban.android.chat.models.Message;

public class MyChatMessage extends ChatMessage{

    public MyChatMessage(long date, String content) {
        super(date, content);
    }
}
