package rickroydaban.android.chat.screens.chat;

import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import rickroydaban.android.chat.callbacks.EmptyCallback;
import rickroydaban.android.chat.screens.chat.ChatView;
import rickroydaban.android.chat.screens.chat.interfaces.RoomSubscription;
import rickroydaban.android.chat.screens.chat.models.ChatMessage;
import rickroydaban.android.chat.utils.LocalPref;
import rickroydaban.android.chat.webservice.Webservice;

public class ChatPresenterImpl implements ChatPresenter {
    private ChatView view;
    private Webservice webservice;
    private Context context;

    private List<ChatMessage> chatItems;
    private ChatAdapter adapter;
    private String roomKey;

    public ChatPresenterImpl(ChatView view, Webservice webservice){
        this.view = view;
        this.webservice = webservice;
    }

    @Override
    public void init(Context context) {
        this.roomKey = "SampleRoomKey";
        this.context = context;
        chatItems = new ArrayList<>();
        adapter = new ChatAdapter(chatItems);
        view.updateAdapter(adapter);
        subscribeToRoomConversation();
    }

    @Override
    public void subscribeToRoomConversation() {
        webservice.subscribeToRoomConversation(roomKey, new RoomSubscription() {
            @Override
            public void onNewMessageAdded(ChatMessage chatMessage) {
                chatItems.add(chatMessage);
                Collections.sort(chatItems, new Comparator<ChatMessage>() {
                    @Override
                    public int compare(ChatMessage chatMessage1, ChatMessage chatMessage2) {
                        return (int) (chatMessage1.getDate()-chatMessage2.getDate());
                    }
                });
                adapter.notifyDataSetChanged();
                view.onGetConversationSuccess();
            }

            @Override
            public void onFailure(String message) {
                view.onGetConversationFailed(message);
            }
        });
    }

    @Override
    public void sendMessage(String message) {
        final long dateSent = new Date().getTime();
        webservice.sendMessage(roomKey, dateSent, LocalPref.get().getKey(), LocalPref.get().getName(), message, new EmptyCallback() {
            @Override
            public void onSuccess() {
                view.onSendMessageSuccess();
            }

            @Override
            public void onFailure(String message) {
                view.onSendMessageFailure(message);
            }
        });
    }

    @Override
    public void logout() {
        FirebaseAuth.getInstance().signOut();
        LocalPref.get().logout();
        view.onLogoutSuccess();
    }
}
