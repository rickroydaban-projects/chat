package rickroydaban.android.chat.screens.chat;

public interface ChatView {
    void onMessageProcessed();

    void updateAdapter(ChatAdapter adapter);

    void sendMessage();

    void logout();

    void onSendMessageSuccess();

    void onSendMessageFailure(String message);

    void onGetConversationSuccess();

    void onGetConversationFailed(String message);

    void onLogoutSuccess();
}