package rickroydaban.android.chat.screens.chat.interfaces;


import rickroydaban.android.chat.screens.chat.models.ChatMessage;

public interface RoomSubscription {
    void onNewMessageAdded(ChatMessage chatMessage);
    void onFailure(String message);
}
