package rickroydaban.android.chat.screens.login;

/**
 * Created by rickroydaban on 01/08/2018.
 */

public interface LoginView {
    void signup();
    void login();
    void onLoginSuccess();
    void onLoginFailed(String message);
    void onUsernameIncorrect(String message);
    void onPasswordIncorrect(String message);
}
