package rickroydaban.android.chat.screens.chat.models;

import rickroydaban.android.chat.models.Message;

public class ChatMessage extends Message {

    public ChatMessage(long date, String content) {
        super(date, content);
    }
}
