package rickroydaban.android.chat.screens.chat;

import android.content.Context;

public interface ChatPresenter {
    void init(Context context);
    void subscribeToRoomConversation();
    void sendMessage(String message);
    void logout();
}
