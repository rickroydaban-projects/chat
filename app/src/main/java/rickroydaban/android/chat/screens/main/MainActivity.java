package rickroydaban.android.chat.screens.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rickroydaban.android.chat.R;
import rickroydaban.android.chat.screens.main.MainView;
import rickroydaban.android.chat.screens.chat.ChatActivity;
import rickroydaban.android.chat.screens.login.LoginActivity;
import rickroydaban.android.chat.screens.signup.SignupActivity;

public class MainActivity extends AppCompatActivity implements MainView {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if(FirebaseAuth.getInstance().getCurrentUser() != null){
            Intent intent = new Intent(this, ChatActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @OnClick(R.id.buttons_signup) public void onSignupClick() {
        navigateToSignup();
    }

    @OnClick(R.id.buttons_login) public void onLoginClick() {
        navigateToLogin();
    }

    @Override
    public void navigateToLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void navigateToSignup() {
        Intent intent = new Intent(this, SignupActivity.class);
        startActivity(intent);
    }
}
