package rickroydaban.android.chat.models;

/**
 * Created by rickroydaban on 01/08/2018.
 */

public class User {
    private String userKey; //since firebase creates users with key-based indexing, we will be using string as data type
    private String username;

    public User(String userKey, String username){
        this.userKey = userKey;
        this.username = username;
    }

    public String getUserKey(){ return userKey; }
    public String getUsername(){ return username; }

}
