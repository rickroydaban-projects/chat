package rickroydaban.android.chat.models;

public class Message {
    private long date;
    private String content;

    public Message(long date, String content){
        this.date = date;
        this.content = content;
    }

    public long getDate(){
        return date;
    }

    public String getContent(){
        return content;
    }
}
