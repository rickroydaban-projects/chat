package rickroydaban.android.chat;

import android.content.Context;

import rickroydaban.android.chat.screens.chat.ChatActivity;
import rickroydaban.android.chat.screens.chat.ChatPresenter;
import rickroydaban.android.chat.screens.chat.ChatPresenterImpl;
import rickroydaban.android.chat.screens.login.LoginActivity;
import rickroydaban.android.chat.screens.login.LoginPresenter;
import rickroydaban.android.chat.screens.login.LoginPresenterImpl;
import rickroydaban.android.chat.screens.signup.SignupActivity;
import rickroydaban.android.chat.screens.signup.SignupPresenter;
import rickroydaban.android.chat.screens.signup.SignupPresenterImpl;
import rickroydaban.android.chat.webservice.FirebaseWebservice;
import rickroydaban.android.chat.webservice.Webservice;

public class DependencyAssembly {
    private static DependencyAssembly instance;

    public static void init(Context context){
        instance = new DependencyAssembly(context);
    }

    public static DependencyAssembly get(){ return instance; }

    private Webservice webservice;

    private DependencyAssembly(Context context){
        this.webservice = new FirebaseWebservice();
    }

    public ChatPresenter chatPresenter(ChatActivity chatActivity){ return new ChatPresenterImpl(chatActivity, webservice); }
    public SignupPresenter signupPresenter(SignupActivity signupActivity){ return new SignupPresenterImpl(signupActivity, webservice); }
    public LoginPresenter loginPresenter(LoginActivity loginActivity){ return new LoginPresenterImpl(loginActivity, webservice); }
}
