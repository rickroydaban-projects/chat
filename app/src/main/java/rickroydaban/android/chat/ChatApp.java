package rickroydaban.android.chat;

import android.app.Application;

import rickroydaban.android.chat.utils.LocalPref;

/**
 * Created by rickroydaban on 01/08/2018.
 */

public class ChatApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        DependencyAssembly.init(this);
        LocalPref.init(this);
    }
}
