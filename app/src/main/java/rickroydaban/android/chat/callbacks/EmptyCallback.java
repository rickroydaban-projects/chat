package rickroydaban.android.chat.callbacks;

/**
 * Created by Rick Royd Aban on 12/04/2018.
 */
public interface EmptyCallback {
    void onSuccess();
    void onFailure(String message);
}
