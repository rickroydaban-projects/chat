package rickroydaban.android.chat.callbacks;

/**
 * Created by Rick Royd Aban on 12/04/2018.
 */
public interface GenericCallback {
    <T> void onSuccess(T result);
    void onFailure(String message);
}
