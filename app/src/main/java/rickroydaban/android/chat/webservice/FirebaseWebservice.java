package rickroydaban.android.chat.webservice;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import rickroydaban.android.chat.callbacks.EmptyCallback;
import rickroydaban.android.chat.callbacks.GenericCallback;
import rickroydaban.android.chat.screens.chat.interfaces.RoomSubscription;
import rickroydaban.android.chat.screens.chat.models.ChatMessage;
import rickroydaban.android.chat.screens.chat.models.MyChatMessage;
import rickroydaban.android.chat.screens.chat.models.TheirChatMessage;
import rickroydaban.android.chat.screens.login.models.AuthUser;
import rickroydaban.android.chat.utils.Constants;
import rickroydaban.android.chat.utils.LocalPref;

public class FirebaseWebservice implements Webservice {
    private final String ROOMS = "rooms";
    private final String USERKEY = "userkey";
    private final String USERNAME = "username";
    private final String DATE = "date";
    private final String CONTENT = "content";

    @Override
    public void login(String username, String password, final GenericCallback callback) {
        final Task task = FirebaseAuth.getInstance().signInWithEmailAndPassword(username, password);
        task.addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                            callback.onSuccess(new AuthUser(user.getUid(), user.getEmail()));
                        }
                    }
                });
        task.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        callback.onFailure(e.getMessage());
                    }
                });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(!task.isComplete()){
                    FirebaseDatabase.getInstance().purgeOutstandingWrites();
                }
            }
        }, Constants.LOAD_TIMEOUT);
    }

    @Override
    public void signup(String username, String password, final EmptyCallback callback) {
        final Task task = FirebaseAuth.getInstance().createUserWithEmailAndPassword(username, password);
        task.addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    callback.onSuccess();
                }
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.onFailure(e.getMessage());
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(!task.isComplete()){
                    FirebaseDatabase.getInstance().purgeOutstandingWrites();
                }
            }
        }, Constants.LOAD_TIMEOUT);
    }

    @Override
    public void subscribeToRoomConversation(final String roomKey, final RoomSubscription roomSubscription) {
        FirebaseDatabase.getInstance().getReference().child(ROOMS).child(roomKey).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
                ChatMessage chatMessage;

                if(value.get(USERKEY).toString().equals(LocalPref.get().getKey())){
                    chatMessage = new MyChatMessage(Long.parseLong(value.get(DATE).toString()), value.get(CONTENT).toString());
                }else{
                    chatMessage = new TheirChatMessage(Long.parseLong(value.get(DATE).toString()), value.get(CONTENT).toString(), value.get(USERNAME).toString());
                }

                roomSubscription.onNewMessageAdded(chatMessage);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                //No implementation since there is no delete capability
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                roomSubscription.onFailure(databaseError.getMessage());
            }
        });
    }

    @Override
    public void sendMessage(String roomKey, long date, String userKey, String username, String message, final EmptyCallback callback) {
        Map<String, Object> chat = new HashMap<>();
        chat.put(DATE, date);
        chat.put(USERKEY, userKey);
        chat.put(USERNAME, username);
        chat.put(CONTENT, message);
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(ROOMS).child(roomKey);
        String key = ref.push().getKey();
        final Task task = ref.child(key).setValue(chat);
        task.addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                callback.onSuccess();
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.onFailure(e.getMessage());
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(!task.isComplete()){
                    FirebaseDatabase.getInstance().purgeOutstandingWrites();
                }
            }
        }, Constants.LOAD_TIMEOUT);
    }
}
