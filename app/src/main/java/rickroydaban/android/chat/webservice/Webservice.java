package rickroydaban.android.chat.webservice;

import android.content.Context;

import java.util.concurrent.Executor;

import rickroydaban.android.chat.callbacks.EmptyCallback;
import rickroydaban.android.chat.callbacks.GenericCallback;
import rickroydaban.android.chat.screens.chat.interfaces.RoomSubscription;

/**
 * Created by rickroydaban on 01/08/2018.
 */

public interface Webservice {
    void login(String username, String password, GenericCallback callback);
    void signup(String username, String password, EmptyCallback callback);
    void subscribeToRoomConversation(String roomKey, RoomSubscription roomSubscription); //added room key in case we want to scale in the future
    void sendMessage(String roomKey, long date, String userKey, String username, String message, EmptyCallback callback);
}
